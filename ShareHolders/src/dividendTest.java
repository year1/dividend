import junit.framework.TestCase;

public class dividendTest extends TestCase 
{
	/*	Test 001
	 * 	Objective:	Test for minimal boundary values for Revenue, Cost, Target
	 * 	Input Revenue = minInt
	 * 	Input Cost = minInt
	 *  Input Target = minInt
	 * 	Expected output = -1
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testcalculateDividend001()
	{
		assertEquals(-1,dividend.calculateDividend(Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE),0.005);
	}
	/*	Test 002
	 * 	Objective:	Test for lower rejected boundary values for Revenue, Cost, Target
	 * 	Input Revenue = 499
	 * 	Input Cost = 499
	 *  Input Target = 499
	 * 	Expected output = -1
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testcalculateDividend002()
	{
		assertEquals(-1,dividend.calculateDividend(499,499,499),0.005);
	}
	/*	Test 003
	 * 	Objective:	Test for lower accepted boundary values for Revenue, Cost, Target
	 * 	Input Revenue = 500
	 * 	Input Cost = 500
	 *  Input Target = 500
	 * 	Expected output = 0
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testcalculateDividend003()
	{
		assertEquals(0,dividend.calculateDividend(500,500,500),0.005);
	}
	/*	Test 004
	 * 	Objective:	Test for upper accepted boundary values for Revenue, Cost, Target
	 * 	Input Revenue = 500'000
	 * 	Input Cost = 500'000
	 *  Input Target = 500'000
	 * 	Expected output = 0
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testcalculateDividend004()
	{
		assertEquals(0,dividend.calculateDividend(500000,500000,500000),0.005);
	}
	/*	Test 005
	 * 	Objective:	Test for upper rejected boundary values for Revenue, Cost, Target
	 * 	Input Revenue = 500'001
	 * 	Input Cost = 500'001
	 *  Input Target = 500'001
	 * 	Expected output = -1
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testcalculateDividend005()
	{
		assertEquals(-1,dividend.calculateDividend(500001,500001,500001),0.005);
	}
	/*	Test 006
	 * 	Objective:	Test for upper rejected boundary values for Revenue, Cost, Target
	 * 	Input Revenue = maxInt
	 * 	Input Cost = maxInt
	 *  Input Target = maxInt
	 * 	Expected output = -1
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testcalculateDividend006()
	{
		assertEquals(-1,dividend.calculateDividend(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE),0.005);
	}
	/*	Test 007
	 * 	Objective:	Test for for 5% dividend.
	 * 	Input Revenue = 1999
	 * 	Input Cost = 1000
	 *  Input Target = 1500 ( Target > ( Revenue - Cost ) )
	 * 	Expected output = 49.95
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	
	public void testcalculateDividend007()
	{
		assertEquals(49.95,dividend.calculateDividend(1999,1000,1500),0.005);
	}
	/*	Test 008
	 * 	Objective:	Test for for 5% dividend rate.
	 * 	Input Revenue = 3001
	 * 	Input Cost = 1000
	 *  Input Target = 1500 ( Target < ( Revenue - Cost ) )
	 * 	Expected output = 200.1
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testcalculateDividend008()
	{
		assertEquals(200.1,dividend.calculateDividend(3001,1000,1500),0.005);
	}
	/*	Test 009
	 * 	Objective:	Test to capture negative profits
	 * 	Input Revenue = 2000
	 * 	Input Cost = 3000
	 *  Input Target = 1500 ( Target < ( Revenue - Cost ) )
	 * 	Expected output = 0
	 *  Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testcalculateDividend009()
	{
		assertEquals(0,dividend.calculateDividend(2000,3000,1500),0.005);
	}
}
