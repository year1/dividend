
public class dividend 
{
	public static double calculateDividend(int revenue, int cost, int target)
	{
		if (revenue < 500 || cost < 500 || target < 500 || revenue > 500000 || cost > 500000 || target > 500000)
		{
			return -1;
		}
		else
		{
			double profit = revenue - cost;
			if (profit > 0)
			{
				if (profit < target)
					return profit*0.05;
				else
					return profit*0.1;
			}
			else
				return 0;
		}
			
	}
}
